describe('Test API', () => {
    beforeEach('log in to the application', () => {
        cy.login()
    })

    it('create new article', () => {
        cy.intercept('POST', '**/articles').as('postArticles')

        cy.contains('New Article').click()
        cy.get('[placeholder="Article Title"]').type('Article title')
        cy.get('[placeholder="What\'s this article about?"]').type('description')
        cy.get('[placeholder="Write your article (in markdown)"]').type('New article very intresting')
        cy.get('button[type="button"]').click()

        cy.wait('@postArticles')
        cy.get('@postArticles').then(xhr => {
            console.log(xhr)
            expect(xhr.response.statusCode).to.equal(200)
            expect(xhr.request.body.article.body).to.equal('New article very intresting')
            expect(xhr.response.body.article.description).to.equal('description')
        })
    })

    it.only('intercept and modify the request and response', () => {
        // cy.intercept('POST', '**/articles', (req) => {
        //     req.body.article.description = "New description intercepted"
        // }).as('postArticles')

        cy.intercept('POST', '**/articles', (req) => {
            req.reply( res => {
                expect(res.body.article.description).to.equal('description')
                res.body.article.description = 'This is modified description'
            })
        }).as('postArticles')

        cy.contains('New Article').click()
        cy.get('[placeholder="Article Title"]').type('Article title')
        cy.get('[placeholder="What\'s this article about?"]').type('description')
        cy.get('[placeholder="Write your article (in markdown)"]').type('New article very intresting')
        cy.get('button[type="button"]').click()

        cy.wait('@postArticles')
        cy.get('@postArticles').then(xhr => {
            console.log(xhr)
            expect(xhr.response.statusCode).to.equal(200)
            expect(xhr.request.body.article.body).to.equal('New article very intresting')
            expect(xhr.response.body.article.description).to.equal('This is modified description')
        })
    })
})