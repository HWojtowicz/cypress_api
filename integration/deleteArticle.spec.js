describe('Delete Article through API', () => {
    beforeEach('log in to the application', () => {
        cy.login()
    })

    it('Create and delete article', () => {
        const bodyRequest = {
            "article": {
                "title": "Article API", 
                "description": "Newly created article", 
                "body": "Nowe", 
                "tagList": []
            }
        }
        cy.get('@tokenOnly').then(token => {

            cy.request({
                url:'http://localhost:3000/api/articles',
                headers: { 'Authorization': 'Token '+token},
                method: 'POST',
                body: bodyRequest
            }).then( response => {
                expect(response.status).to.equal(200)
            })

            cy.contains('Global Feed').click()
            cy.reload()
            cy.contains('Global Feed').click()
            cy.get('.article-preview').contains('Article API').click()
            cy.get('.article-actions').contains('Delete Article').click()

            cy.request({
                url: 'http://localhost:3000/api/articles?limit=10&offset=0',
                headers: { 'Authorization': 'Token '+token},
                method: 'GET',
            }).its('body').then( body => {
                expect(body.articles[0].title).not.to.equal('Article API')
            })
        })
    })
})