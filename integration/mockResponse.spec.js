describe('Mocking API Response', () => {
    beforeEach('log in to the application', () => {
        //cy.intercept('GET', '**/tags', {fixture:'tags.json'})
        cy.intercept({method:'Get', path: 'tags'}, {fixture:'tags.json'})
        cy.login()
    })

    it.only('Mocking API response for tags', () => {
        cy.get('.tag-list')
        .should('contain', 'cypress')
        .and('contain', 'test')
        .and('contain', 'automation')
    })

    it('Mocking API response for articles', () => {
        cy.intercept('GET', '**/articles/feed*', {"articles":[],"articlesCount":0})
        cy.intercept('GET', '**/articles*', {fixture:'articles.json'})
        cy.contains('Global Feed').click()
        cy.get('.article-preview button').then( buttonLikes => {
            expect(buttonLikes[0]).to.contain(1)
            expect(buttonLikes[1]).to.contain(5)
        })

        cy.fixture('articles').then( file => {
            const articleLink = file.articles[1].slug
            cy.intercept('POST', '**/articles/'+articleLink+'/favourite', file)
        })

        cy.get('.article-preview button')
        .eq(1)
        .click()
        .should('contain', 6)
    })
})